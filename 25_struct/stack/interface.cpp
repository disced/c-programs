#include "interface.h"


void
banner ( void ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t '< < < Stack > > >'";
    system ( command );
}

void
print_stack ( struct TStack *st ) {
    banner();
    printf ( "\t\tLa pila\n" );
    for ( int i=st->summit-1; i>=0; i-- )
        printf ( "\t\t%2i\n", st->data[i] );
}

void
print_err ( const char *type ){
    fprintf (stderr, RED "\tFailed to %s in the stack.\n\n", type );
}
