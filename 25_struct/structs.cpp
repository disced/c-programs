#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct car {
    int cc;
    char year[5],
    marca[20],
    modelo[40];
};

void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}

int
main (int argc, char *argv[]) {
    banner ( "Estructuras" );

    int cc_buff;
    struct car Coche1 = {
        1999,
        "2015",
        "BMW",
        "320"
    };
    struct car Coche2;

    printf ( "Datos del coche\n\tCC = %i\n\tAño = %s\n\tMarca = %s\n\tModelo = %s\n",
            Coche1.cc,
            Coche1.year,
            Coche1.marca,
            Coche1.modelo );

    printf ( "Coche 2\nCC:\n" );
    scanf ( " %i", &cc_buff );
    Coche2.cc = cc_buff;

    printf ( "Coche2: %i\n", Coche2.cc );

    return EXIT_SUCCESS;
}
