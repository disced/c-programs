#include "interface.h"

int
main ( int argc, char *argv[] ) {
    struct TStack stack;
    srand ( time(NULL) );

    init ( &stack );

    for ( int i=0; i<11; i++ )
        push ( &stack, rand() % 10 );

    print_stack ( &stack );
    pop ( &stack );

    if ( stack.fail )
        print_err ( "pop" );

    getchar();
    print_stack ( &stack );
    printf ( "Se ha ejecutado pop y peek:\n\t%i\n", peek (&stack) );

    destroy ( &stack );
    return EXIT_SUCCESS;
}
