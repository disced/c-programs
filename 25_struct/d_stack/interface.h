#ifndef __INTERFACE_H__
#define __INTERFACE_H__

#include "stack.h"
#include "colors.h"

#ifdef __cplusplus
extern "C" {
#endif

void banner      ( void );
void print_stack ( struct TStack *st );
void print_err   ( const char *type );

#ifdef __cplusplus
}
#endif

#endif
