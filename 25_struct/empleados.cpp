#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include "colors.h"

struct TEmpleado {
    char nombre   [50];
    char apellido [90];
    int edad;
    double salario;
};

void
banner ( const char *title ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

int
empleado_mes ( struct TEmpleado **l, unsigned size ) {
    char nombre_in[30], apellido_in[30];
    int rv_nom, rv_ap, i;
    printf ( BG_BLUE WHITE BOLD "Elige empleado del mes:\n\n" );
    for ( i=0; i<size; i++ )
        printf ( YELLOW BOLD BG_BLACK "\t%i.- %s %s con sueldo de %.2lf\n\n" RESET,i+1, (*(l+i))->nombre, (*(l+i))->apellido, (*(l+i))->salario );
    //                                                                                   l[i] -> nombre
    printf ( BG_BLUE WHITE BOLD "\nNombre y Apellido: " );
    scanf  ( " %s %s", nombre_in, apellido_in );

    i=0;
    do {
        rv_nom = strcasecmp ( nombre_in,   l[i]->nombre   );
        rv_ap  = strcasecmp ( apellido_in, l[i++]->apellido );
        } while ( rv_nom != 0 && rv_ap != 0 );

    if ( (rv_nom + rv_ap) == 0)
        return --i;
    else return -1;
}

void
aumenta_sueldo ( struct TEmpleado *emp ) { emp->salario += 1 * emp->salario / 100; }

void
imprime_empleado ( struct TEmpleado *emp ) {
    printf ( RESET BG_BLACK BOLD GREEN "\n\t%s %s de %i años, ahora cobra %.2lf\n\n",
            emp->nombre,
            emp->apellido,
            emp->edad,
            emp->salario );
}
void
error () {
    printf ( BG_RED WHITE BOLD "\n\t¡ No existe el empleado !\n\n" );
}

int
main ( int argc, char *argv[] ) {
    banner ( " ' < < <  Empleados  > > > ' " );
    int size, elegido;

    // En c++ se puede hacer: 'TEmpleado antonio = {...};'
    struct TEmpleado antonio = { "Antonio", "Jimenez",  43, 1999.23 };
    struct TEmpleado jacinto = { "Jacinto", "Gonzalez", 31, 3200.97 };

    // Puntero a array de struct TEmpleado.
    struct TEmpleado *lista_empleados[] = { &antonio, &jacinto },
                     *del_mes;

    size    =  sizeof(lista_empleados) / sizeof(TEmpleado*);
    elegido = empleado_mes ( lista_empleados, size );

    if ( elegido >= 0 ) {
        del_mes = lista_empleados[elegido];
        aumenta_sueldo ( del_mes );
        imprime_empleado ( del_mes );
    } else error();

    return EXIT_SUCCESS;
}
