#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>


// Creamos el "molde" para estructuras, no está en RAM.
struct TCordenada {
    double x;
    double y;
};

const char *TipoDato[] = { "posicion", "velocidad", NULL };

void
banner ( void ) {
    const char *title = " '< < < Posicion y Velocidad > > >' ";
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

void
ask_user ( struct TCordenada *pos, struct TCordenada *vel ) {
    printf ( "Posicion inicial\nX: " );
    scanf ( " %lf", &pos->x );
    printf ( "Y: " );
    scanf ( " %lf", &pos->y );
    printf ( "Velocidad: " );
    scanf ( " %lf", &vel->x ) ;

}
void
print_pos ( struct TCordenada pos, struct TCordenada vel ) {
    while ( true ) {
        banner ();
        printf ( "Avanzando Posición:\n\n\tX: %.1lf\tY: %.1lf\n\n", pos.x, pos.y );
        pos.x  += vel.x;
        pos.y  += vel.x;
        usleep (1000000);
    };
}

int
main ( int argc, char *argv[] ) {
    banner ();
    struct TCordenada velocidad;
    struct TCordenada posicion;

    ask_user ( &posicion, &velocidad );
    print_pos ( posicion, velocidad);

    return EXIT_SUCCESS;
}
