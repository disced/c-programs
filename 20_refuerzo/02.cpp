#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define LIM 100

void
banner ( const char *titulo ) {
    char comando[200] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}
int
main (int argc, char *argv[]) {
    banner ( "Ejercicio 2" );
    int result;

    // FOR
    /*
       for ( int i=2; i<LIM; i+=3 )
       result += i;
       printf( "El resultado es: %i\n", result );
       */

    // WHILE
    /*
       int i = 2;
       while ( i < LIM ) {
       result += i;
       i += 3;
       }
       printf ( "El resultado es: %i\n", result );
    */

    //DO WHILE
    int i = 2;
    do {
        result += i, i += 3;
    } while ( i<LIM );

    printf( "El resultado es: %i\n", result );
    return EXIT_SUCCESS;
}
