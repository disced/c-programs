#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define LIM 100
void
banner ( const char *titulo ) {
    char comando[200] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}
int
main (int argc, char *argv[]) {
    banner ( "Ejercicio 03" );
    int user_num, result = 0;
    printf ( "Introduce numero de inicio:\n" );
    scanf ( " %i", &user_num );

    for ( int i=user_num; i < (user_num + LIM); i+=3 )
        result += i;
    printf ( "Resultado: %i\n", result );
    return EXIT_SUCCESS;
}
