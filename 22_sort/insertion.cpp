#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void
banner ( const char *titulo ) {
    char comando[250] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}

void
sort ( int tot, unsigned *num ) {
    unsigned temp, pst;                                     // temp guarda el numero actual, pst la posicion
    for ( int i=0; i<tot; i++ ) {                           // Recorre todos los numeros
        temp = num[i], pst = i;

        while (( pst > 0) && ( num[pst-1] > temp )) {       // Comprueba si el numero anterior al que estamos es mayor
            num[pst--] = num[pst-1];                        // si es mayor, guarda en el actual su valor y comprueba el
        }                                                   // anterior...
        num[pst] = temp;                                    // Guarda el numero en la posicion correspondiente
    }
}
void
imprime ( int tot, unsigned *num ) {                        // Le paso el puntaro a imprimir, como no haré cambios
    for ( int i=0; i<tot; i++ )                             // al puntero, en main no digo &numbers ya que es un puntero
        printf ( "%u ", *( num+i ) );                       // digo solo numbers
    printf("\n");
}
unsigned
mediana ( int tot, unsigned *num ) {
    int media = tot / 2;
    return num[media];
}

int
main (int argc, char *argv[]) {
    banner ( "Insertion Sort" );
    unsigned *numbers = NULL;
    int buffer, summit = 0, media;

    // Entrada
    // No he conseguido hacerlo en una funcion, me lio mucho con los punteros dobles
    printf ( "\t\t\tNumeros\n\t\tTermina con un negativo\n");
    do {
        numbers = ( unsigned* ) realloc ( numbers, ( summit+1 ) * sizeof( unsigned ) );
        scanf ( "%i", &buffer );
        *( numbers + summit++ ) = buffer;

    } while ( buffer > 0);
    numbers = ( unsigned* ) realloc ( numbers, ( summit-1 ) * sizeof( unsigned ) );
    summit--;

    // Calculos
    sort( summit, numbers );
    media = mediana ( summit, numbers );

    // Salida
    banner ( "Insertion Sort" );
    printf( "Numeros Introducidos: %i\n", summit );
    imprime ( summit, numbers );
    printf ( "Mediana: %u\n", media );
    free( numbers );

    return EXIT_SUCCESS;
}
