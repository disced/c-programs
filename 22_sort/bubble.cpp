#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void
banner ( const char *titulo ) {
    char comando[250] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}
int
insert ( unsigned **num ) {                                        // Introducir numeros de forma dinamica
    int s = 0, buff;                                               // conseguido con una funcion!
    do {
        *num = ( unsigned* ) realloc ( *num, (s+1) * sizeof(unsigned) );
        scanf ( " %i", &buff );
        *( *num + s++ ) = (unsigned) buff;
    } while ( buff > 0);
    return s - 1;
}
void
sort ( unsigned *nums, int total ) {
    int temp,
        pst;
    pst = total;
    while ( pst > 0) {                                              // Ordena hasta el ultimo numero
        for ( int i=0; i<(pst-1); i++ ) {                            // despues ordena hasta el ultimo-1...
            temp = nums[i];
            if ( nums[i] > nums[i+1] ) {
                nums[i] = nums[i+1];
                nums[i+1] = temp;
            }
        }
        pst--;
    }
}
void
printn (unsigned *nums, int total) {
    banner ( "Bubble Sort" );
    printf ( "Ordenados:\n\t\t" );
    for ( int i=0; i<total; i++ )
        printf ( "%u ", *(nums+i) );
    printf ( "\n\n" );
}
int
main (int argc, char *argv[]) {
    unsigned *num = NULL;
    int total;

    banner ( "Bubble Sort" );
    // Entrada
    printf ( "\t\t\t    Numeros\n\t\tFinaliza con '0' o numero negativo\n" );
    total = insert ( &num );
    // Calculos
    sort ( num, total );
    // Salida
    printn ( num, total );

    return EXIT_SUCCESS;
}
