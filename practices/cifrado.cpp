#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void banner ( const char *titulo ) {
    char comando[50] = "toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( "clear" );
    system ( comando );
}

const char *input ( const char *phrase ){
    /** Retorna la frase que introducimos **/
    return phrase;
}

void encrypt ( const char *phrase, int size ) {
    /** Cifra la frase 'phrase' que le pasamos por parametro, en este caso
     * le pasamos el retorno de la funcion 'input', y un numero
     * entero para crear el cifrado cesar 'size'
     */

    /** Variable tipo array del tamaño de la frase introducida
     * gracias al valor de retorno de la funcion 'strlen', se
     * le suma 1 ya que 'strlen' devuelve el tamaño total sin
     * contar con el caracter '\0'
     */
    char new_phrase [strlen(phrase)+1];

    /** Copia el contenido del parametro 'phrase' a la
     * nueva variable de tipo array 'new_phrase'
     *
     * No uso 'strncpy' ya que la variable 'new_phrase' tiene el mismo
     * tamaño que 'phrase'
     */
    strcpy ( new_phrase, phrase );
        printf ( "Frase a cifrar:\n\t%s\n", phrase);

    /** Bucle para recorrer el tamaño total del array new_phrase
     * A cada celda del array se le suma el valor de size
     */
    for ( int i=0; i<strlen(new_phrase); i++ )
        new_phrase[i] += size;
    printf ( "Frase cifrada:\n\t%s\n", new_phrase );
}

int main (int argc, char *argv[]) {
    banner ( "Cifrado Cesar" );
    encrypt ( "Vamos a cifrar con una clave secreta\0 13", 13 );
    return EXIT_SUCCESS;
}

