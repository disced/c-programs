#include <stdio.h>

// Programa que pinta en consola 4 tablas de verdad And, Or, Not y Xor


int main () {
  int opc; /*Variable Opción*/
  printf ("Tablas de Verdad\n");
  /*Print de las opciones */
  printf (" AND &: 1\n");
  printf (" OR |: 2\n");
  printf (" NOT !: 3\n");
  printf (" XOR ^: 4\n");
  printf (" \n");
  /*Input del usuario */
  scanf ("%d", &opc);

  /*Funcion switch de la variable opc */

  while (opc <5) {
     switch (opc) {
        case 1: printf ("Tabla AND\n");
                printf ("\t0 & 0 = 0\n\t0 & 1 = 0\n\t1 & 0 = 0\n\t1 & 1 = 1\n");
                printf ("Nueva Opcion\n5 Salir\n");
                scanf ("%d", &opc);
                break;
        case 2: printf ("Tabla OR\n");
                printf ("\t0 | 0 = 0\n\t0 | 1 = 1\n\t1 | 0 = 1\n\t1 | 1 = 1\n");
                printf ("Nueva Opcion\n5 Salir\n");
                scanf ("%d", &opc);
                break;
        case 3: printf ("Tabla NOT\n");
                printf ("\t!0 = 1\n\t!1 = 0\n");
                printf ("Nueva Opcion\n5 Salir\n");
                scanf ("%d", &opc);
                break;
        case 4: printf ("Tabla XOR\n");
                printf ("\t0 ^ 0 = 0\n\t0 ^ 1 = 1\n\t1 ^ 0 = 1\n\t1 ^ 1 = 0\n");
                printf ("Nueva Opcion\n5 Salir\n");
                scanf ("%d", &opc);
                break;
        default: printf ("Has introducido un número incorrecto\n");
    }
  }

 printf ("Numero no valido\n");
}
