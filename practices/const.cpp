
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define PI 3.14
#define LETRAA 'A'
#define SALUDO "Hello World"

char banner ( char titulo[] ) {
    char comando[50] = "toilet -froman -F crop:border:gay -w 130 ";
    strcat ( comando, titulo );
    system ( "clear" );
    system ( comando );
}

int main (int argc, char *argv[]) {

    /*Banner title*/

    char titulo[] = "Constantes";
    banner ( titulo );

    /* Finish Banner*/

    const char *dia1 = "Lunes";
    int year = 2019;
    char Year = (char) year;

    enum DiasSemana  { lunes, martes, miercoles, jueves, viernes, sabado, FIN };

    const char *dias[] = {
        "Lunes",
        "Martes",
        "Miercoles",
        "Jueves",
        "Viernes",
        "Sabado",
        NULL
    };

    printf ( "%s\nOffset: %p\n", dia1, &dia1 );

    for (int i = 0 ; i < FIN; i++) {
        printf ( "%i.-%s\n",i + 1, dias[i] );
    }
    printf ("\n%.2f\n%c\n%s\n", PI, LETRAA, SALUDO);
    printf ("%i\n%c\n%i\n", Year, Year, year);


    return EXIT_SUCCESS;
}
