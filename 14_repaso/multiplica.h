#ifndef __MULTIPLICA_H__
#define __MULTIPLICA_H__

#ifndef __cplusplus
extern "C"
{
#endif

int mult (int *n1, int *n2);

#ifndef __cplusplus
}
#endif

#endif
