#ifndef __RAISED_H__
#define __RAISED_H__

#ifndef __cplusplus
extern "C"
{
#endif
    int raised (int base, int exp);
#ifndef __cplusplus
}
#endif

#endif
