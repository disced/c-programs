#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FIN 114
#define ASCII 0x8

void
banner ( const char *titulo ) {
    char comando[100] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}

int
main (int argc, char *argv[]) {
    banner ( "Cuenta Letras" );
    // Entrada
    char frase[] = "Faltan cosas...";
    char result[ strlen(frase) ];

    // Calculos
    int i =0;
    while (i < strlen(frase) ) {
        for (int j=0, ascii=ASCII; j<FIN; j++, ascii++) {
            if ( frase[i] == ascii ) {
                result[i] = ascii;
                i++, j=0, ascii=ASCII;
            }
        }
    }
    // Guardar en total[X] el total de 'C' en total[X+1] el total de 'u' y así sucesivamente en función de las letras existentes

    // Salida
    for (int j=0; j<strlen(frase); j++)
        printf ( "frase[%02i] ---- %c\n", j, result[j] );
    return EXIT_SUCCESS;
}
