#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXDAT 11

const char *tipos[] = {
    "Tinyint",
    "Smallint",
    "Mediumint",
    "Int",
    "Bigint",
    "Char",
    "Varchar",
    "Tinytext",
    "Text",
    "Mediumtext",
    "Longtext",
    NULL
};
const char *size[] = {
    "1",
    "2",
    "3",
    "4",
    "8",
    "1",
    "2",
    "255",
    "64.000",
    "16.000.000",
    "4.000.000.000"
};

void banner ( const char *titulo ) {
    char comando[50] = "toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( "clear" );
    system ( comando );
}

int main (int argc, char *argv[]) {
    // Entrada
    // Calculos
    // Salida
    banner ( "Tipo Datos BBDD" );
    for ( int i=0; i<MAXDAT; i++ )
        printf ( "%s : %s Bytes\n\n", tipos[i], size[i] );



    return EXIT_SUCCESS;
}

