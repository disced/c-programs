/**
 * @brief Introduces una frase y una letra que esté en la misma
 *        y muestra la posicion de la letra en la frase
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 255

void
banner ( const char *titulo ) {
    char comando[50] = "toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( "clear" );
    system ( comando );
}
/**
 * @brief Averigua el total de la letra seleccionada
 * @details Se pasa como parametros una frase y una letra
 *          y a continuación devuelve cuantas letras 'X'
 *          hay dentro de la frase.
 *
 * @return int r
 * @param frase puntero const char, la frase en cuestión.
 * @param letra char, la letra a averiguar.
 * @var r valor de retorno, el total de la letra dentro de la frase
 * @var q contador para el bucle for
 *
 */
int
total_letra ( const char *frase, char letra ){
    int r=0;
    for ( int q=0; q<strlen(frase); q++ ) {
        if ( frase[q] == letra ) {
            r++;
        }
    }
    return r;
}

/**
 * @brief Averigua la posicion de cada letra
 * @details Una vez averiaguada la posición del la letra 'x' en la frase se guarda la posición
 *          en un array de tipo int para imprimir más tarde.
 *
 * @return void
 * @param frase puntero char, la frase en cuestión.
 * @param letra char, la letra para averiguar la posición.
 * @var i int se usa para la condición del bulce y para especificar una posicón en frase.
 * @var j int se usa para especificar la posición en array.
 * @var array[] int guarda en [j] posición la posicion de 'x' letra dentro de la frase.
 * @var q int se usa para la condición del bucle for en la parte de salida
 *
 */
void
averigua_letra ( char *frase, char letra ){
    int i=0, j=0, array[ total_letra(frase, letra) ];
    while ( i < strlen(frase) ) {
        /* Bucle que recorre el array 'frase' y compara si en cada posición está la letra.
         * Si en X posicion del array está la letra se añade al array int en que posicion
         * se encuentra la letra
         */
        if ( frase[i] == letra) {
            array[j] = i;
            j++;
        }
        i++;
    }
    printf ( "\n" );
    for ( int q=0; q<j; q++ )
        printf ( "frase[%02i] ┉┉┉┉┉❯ '%c'\n",
                array[q],
                letra
               );
    printf ( "\n" );
}
/* Introduces una frase y una letra */
char
*input_frase ( char *frase ) {
    printf ( "Frase:\n" );
    fgets ( frase, MAX, stdin );
    return frase;
}
char
input_letra ( char letra ){
    printf ( "\nLetra:\n" );
    scanf ( " %c", &letra );
    return letra;
}

int
main ( int argc, char *argv[] ) {
    banner ( "Frase y letra" );
    char frase[MAX], letra; /* Se usan de forma global para no perder el valor de retorno cuando finaliza una funcion */
    char *fras, let; /* Guardan los valores de retorno de las funciones input_frase input_letra */

    fras = input_frase( frase ), let = input_letra( letra );

    averigua_letra( fras, let );

    return EXIT_SUCCESS;
}
