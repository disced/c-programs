#ifndef __CALC_H__
#define __CALC_H__

#ifdef __cplusplus
extern "C" {
#endif
int primo (int num);
void primos_tope (int tope);
void primos_hasta (int fin);
#ifdef __cplusplus
}
#endif
#endif
