#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define TOT 99999
#define DIF 2
/**
 * @brief Imprime un banner como titulo del programa
 * @details Se llama a la función y se pasa como parametro un/a char/frase
 *          se une al final del array 'comando' el parametro 'titulo', y se
 *          ejecuta el comando mediante la función 'system'
 *
 * @return Void
 * @param titulo Puntero constante de tipo char
 * @var comando Array char con el comando a ejecutar
 */
void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}


int
main (int argc, char *argv[]) {
    banner ( "Criba E." );
    unsigned total[TOT];

    // Asignamos en total[0] --> 2, total[1] --> 3
    for ( int i=DIF; i<TOT-DIF; i++)
        total[i-DIF] = i;

    // Calculos
    //
    // Entramos en el primer numero: 2,
    // despues recorremos todos los demás a partir del 2
    // haciendo el modulo de x%2 y si el resultado es 0
    // asignamos a esa poscion un 0, si no aumentamos x
    // y volvemos a ejecutar la misma operacion.
    //
    // Cuando termina con el 2, comienza con el tres de
    // igual manera y así ...


    // Falta comprobar cuando ha terminado, para ello:
    // Si total[i]^2 > TOT-DIF : termina

    printf ( "Calculando...\n" );
    for (int i=0, k=DIF; i<TOT-DIF; i++, k++)
        if ( total[i] != 0 )
            for (int j = k-1; j<TOT-2; j++)
                if ( total[j] % k == 0 )
                    total[j] = 0;

    banner ("Criba E.");
    for (int i=0; i<TOT-DIF; i++)
        if ( total[i] != 0 )
            printf ( "%i ", total[i]);
    printf ( "\n" );

    return EXIT_SUCCESS;
}
