#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/**
 * @brief Imprime un banner como titulo del programa
 * @details Se llama a la función y se pasa como parametro un/a char/frase
 *          se une al final del array 'comando' el parametro 'titulo', y se
 *          ejecuta el comando mediante la función 'system'
 *
 * @return Void
 * @param titulo Puntero constante de tipo char
 * @var comando Array char con el comando a ejecutar
 */
void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}


int
main (int argc, char *argv[]) {
    banner ( "Prioridades" );
    int a=10, b=5, c;
    c = (a*b) +3;
    printf("%i\n", c);

    c=b+1, a*a;
    printf("%i\n", c);

    c = (b+1, a+1);
    printf("%i\n", c);

    c = a<<=1, b<<=1;
    printf("%i\n", c);
    printf("%i\n", a);
    printf("%i\n", b);


    return EXIT_SUCCESS;
}
