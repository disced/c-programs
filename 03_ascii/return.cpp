#include <stdio.h>
#include <unistd.h>

/**
 * Learn the \r expression and view on gdb the fflush (stdout) function
 */

int main () {
    int numero = 0;
    printf("%i\n", numero);
    for (numero = 0; numero<100; numero++){
        printf("\r%i", numero);
        fflush (stdout);
    }
        printf("\n");

    return 0;
}
