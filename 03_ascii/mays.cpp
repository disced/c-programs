#include <stdio.h>

#define SALTO ( 'a' - 'A' )

int main () {
    /**
     * Variable char
     */
    char letra, nueva_letra;
    printf("Introduce letra para pasar a minúscula/mayúscula\n");
    /**
     * Call scanf function and save the value in letra position memory "&"
     */
    scanf("%c", &letra);

    /**
     * Conditional that compares if the var letra is minuscule
     * Then add 0x20 to letra and print new char 
     */
    if ( letra >= 0x61 && letra <= 0x7A ) {
        nueva_letra = letra-SALTO;
        printf("'%c' en minúscula pasa a '%c' mayúscula\n", letra, nueva_letra);
    }
    /**
     * Conditional else if that compares is the var letra is capital letter
     * Then sub 0x20 to letra and print new char
     */
    else if ( letra >= 0x41 && letra <= 0x5A ) {
        nueva_letra = letra+SALTO;
        printf("'%c' en mayúscula pasa a '%c' minúscula\n", letra, nueva_letra);
    }
    /**
     * Else the var letra is not a letter
     */
    else {
        printf("%c no es una letra\n", letra);
    }

    return 0;
}
