#include <stdio.h>
#include <stdlib.h>
#define BARAJA 14

int main (int argc, char *argv[]) {
    /*
       char picas[] = "🂡", corazon[] = "🂱", diamante[] = "🃁", trebol[] = "🃑";

       for ( int i = 0; i<BARAJA; i++ ){
       printf(" %s  %s  %s  %s \n\n", picas, corazon, diamante, trebol);
       picas[3]++;
       corazon[3]++;
       diamante[3]++;
       trebol[3]++;
       }
       */

    char carta[] = "🂡";

    for (int i=0 ; i<BARAJA; i++){
        /**
         * Print Ace of Spades 🂡
         */
        printf("%s \n\n", carta);

        /**
         * Print Ace of Hearts 🂱
         */
        carta[3]+=16;
        printf("%s \n\n", carta);

        /**
         * Print Ace of Diamonds 🃁
         */
        carta[2]++;
        carta[3]-=48;
        printf("%s \n\n", carta);

        /**
         * Print Ace of Clubs 🃑
         */
        carta[3]+=16;
        printf("%s \n\n", carta);
    }

    return EXIT_SUCCESS;
}
