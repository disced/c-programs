#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void banner ( char titulo[] ) {
    char comando[50] = "toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( "clear" );
    system ( comando );
}

/* Function f */
double f ( double x ) {
    return x * x -3;
}


int main (int argc, char *argv[]) {

    /*Banner title*/
    char titulo[] = "Funciones";
    banner ( titulo );
    /* Finish Banner*/

    for ( double x = -5; x<5; x+=0.5 )
        printf ( "x: %.2lf, f(x): %.2lf\n", x, f(x) );

    return EXIT_SUCCESS;
}

