#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
    /**
     * Create variables 
     */
    int lado,i,a;

    /**
     * Input side to print square
     */
    printf("Introduce Lado: ");
    scanf(" %i", &lado);
    
    for (i = 0; i<lado; i++){
        for (a = 0; a<lado; a++)
            printf("⏹ ");
        printf("\n");
    }    

    return EXIT_SUCCESS;
}
