#include <stdio.h>
#include <stdlib.h>

#define ASTERISCO 0x2A
#define BASE 10

int main (int argc, char *argv[]) {
    // imprime un asterisco y salto de linea
    for (int i = 0; i<=BASE; i++)
        printf("%c", ASTERISCO);
    printf("\n");

    return EXIT_SUCCESS;
}
