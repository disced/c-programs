#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_ERROR .001

void banner ( char titulo[] ) {
    char comando[50] = "toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( "clear" );
    system ( comando );
}

int main (int argc, char *argv[]) {

    /*Banner title*/

    char titulo[] = "Compara Reales";
    banner ( titulo );

    /* Finish Banner*/

    double user_number;

    printf ( "Number: " );
    scanf ( " %lf", &user_number );

    if ( user_number >= 3. -MAX_ERROR &&
      user_number <= 3. + MAX_ERROR ) 
        printf ( "Para mí es un 3 a todos los efectos. \n" );
    else 
        printf ( "Para mi no es un 3. \n" );

    return EXIT_SUCCESS;
}

