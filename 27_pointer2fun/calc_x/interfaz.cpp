#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char *funciones[] = {
    "x * 2²",
    "2 * x²",
    "x² * x!",
    "(x * 10) / x²"
};

void
banner ( const char *title ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

void
print_opt ( void ) {
    printf ( "Elige función\n\n" );
    for (int i=0; i<sizeof(funciones) / sizeof(char*); i++)
        printf ( "%i.- %s \n", i+1, funciones[i] );
    printf ( "\n\n" );
}

void
print_err(void) {
    fprintf (stderr,"La opción deben estar entre 1-4\n\n");
    exit (EXIT_FAILURE);
}
