#include <stdio.h>
#include <stdlib.h>
#include "interfaz.h"
#include "calc.h"

int
main ( int argc, char *argv[] ) {
    banner ( " ' < < <  Calcula X  > > > ' " );
    double x, res,
           (*func[]) (double) = { &fun_1, &fun_2, &fun_3, &fun_4 };
    unsigned opc;

    print_opt();
    scanf ( "%u", &opc );

    if ( opc < 1 || opc > 4 )
        print_err();


    // Lee X
    printf ( "Valor para X:\n" );
    scanf ( "%lf", &x );

    res = ( *func[opc-1] ) (x);

    printf ( "Valor de x %.1lf en la función %s = %.2lf\n\n",
            x,
            funciones[opc-1],
            res );

    return EXIT_SUCCESS;
}
