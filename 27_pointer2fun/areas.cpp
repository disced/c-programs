#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>

enum TArea           { triangle, rectangle , MAX };
const char *name[] = { "triangulo", "rectangulo" };
const char *command;

double triangle_area  ( double b, double h ) { return (b * h) / 2; }
double rectangle_area ( double b, double h ) { return b * h; }

void
banner ( const char *title ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

void
p_usage ( FILE *output ) {
    int ex;
    fprintf ( output, "\n\
            Uso:    %s  [-b <num> & -a <num>] [-f tri/rec] [-h]\n\n\
            Argumentos        Descripción\n\
            ------------------------------\n\
            -b                Base, lleva un numero a continuación: -b 3\n\
            -a                Altura, lleva numero a continuación:  -a 5\n\
            -f <tri/rec>      Figura, triangulo o rectangulo respectivamente\n\
            -h                Help.\n\n", command );


    output == stderr ? ex = 1: ex = 0;
    exit(ex);
}

int
main ( int argc, char *argv[] ) {
    banner ( " ' < < <  Areas  > > > ' " );

    double (*fun[MAX]) (double, double) = { &triangle_area, &rectangle_area };            // Puntero a array de funciones.
    double res, base, height;
    enum TArea opc = triangle;
    unsigned arg;


    command = argv[0];  // Modifica la varible global.

    opterr = 0;         // En 0 no aparecen mensajes de error de getopt, solo los mios.
    while ( (arg = getopt ( argc, argv, "b:a:f:h")) != -1 ) {
        switch (arg) {
            case 'b':   // Convierte a double lo que hay en optarg, en este caso
                        // es el argumento del parametro 'b'.
                base = atof (optarg);
                break;
            case 'a':
                height = atof (optarg);
                break;
            case 'h':
                p_usage (stdout);
                break;
            case 'f':
                if ( strcmp(optarg, "rec") == 0 )   // Comprueba si el argumento de 'f' es "rec".
                    opc = rectangle;
                break;
            case '?':
                if ( optopt == 'b' || optopt == 'a' ) {
                    printf ( "Base o Altura necesitan un numero\n\n" );
                    p_usage (stderr);
                }
                else if ( optopt == 'f' ){
                    printf ( "Con la opcion -f debes especificar figura: -f tri\n\n" );
                    p_usage (stderr);
                }
                break;
            default:
                p_usage(stdout);
        }
    }

    res = (*fun[opc]) ( base, height ) ;

    // Salida
    printf ( "\tArea del %s\n\n\tBase: %.1lf\n\tAltura: %.1lf\n\tArea: %.1lf\n\n",
            name[opc],
            base,
            height,
            res );

    return EXIT_SUCCESS;
}
