#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void banner ( char titulo[] ) {
    char comando[50] = "toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( "clear" );
    system ( comando );
}

int main (int argc, char *argv[]) {

    /*Banner title*/

    char titulo[] = "Nombre";
    banner ( titulo );

    /* Finish Banner*/

    char name[20];
    int vez;
    printf ( "Introduce un nombre a imprimir: " );
    scanf ( " %s", name );
    printf ( "Introduce número de veces a imprimir %s: ", name );
    scanf ( " %i", &vez );

    for ( int i = 0; i<vez; i++ )
        printf ("%i.- Your name is: %s\n", i+1,  name);

    return EXIT_SUCCESS;
}

