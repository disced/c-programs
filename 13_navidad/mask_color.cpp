#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define R 2
#define G 1
#define B 0
#define COLS 3

const char *colores[] = {
    "Azul",
    "Verde",
    "Rojo"

};

void banner ( const char *titulo ) {
    char comando[50] = "toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( "clear" );
    system ( comando );
}

void tipo_color( const char color[COLS] ) {
    /** Imprime el color elegido **/
    printf ( "Valor para %s\n", &color[B] );
}

int valor_color( int position ) {
    /** Imprime el color elegido para introducir un valor para el mismo y retornarlo a una nueva funcion **/
    unsigned int buff;
    tipo_color ( colores[position] );
    printf ( "Introduce valor:\n" );
    scanf (" %u", &buff);
    return buff;
}

int valor_mask( int position ) {
    /** Imprime el color elegido para introducir un valor para calcular la mascara en una nueva funcion **/
    unsigned int buff;
    tipo_color ( colores[position] );
    printf ( "Introduce valor para mascara:\n" );
    scanf (" %u", &buff);
    return buff;
}


int mask ( int position ) {
    /** Con el valor de retorno de valor_color() y valor_mask() hace el XOR ^ y retorna el resultado **/
    int new_value;
       new_value = valor_color (position);
       new_value = new_value ^ valor_mask (position);
       return new_value;
}

int main ( int argc, char *argv[] ) {
    unsigned char rgb[COLS];

    banner ( "Mask Color" );
    for ( int i=B; i<COLS; i++ )
        printf ( "Calculo finalizado: %i\n", mask (i) );




    return EXIT_SUCCESS;
}

