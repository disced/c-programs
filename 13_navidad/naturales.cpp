#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 20
void banner ( char titulo[] ) {
    char comando[50] = "toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( "clear" );
    system ( comando );
}

int main (int argc, char *argv[]) {

    /*Banner title*/
    char titulo[] = "Numeros Naturales";
    banner ( titulo );
    /* Finish Banner*/

    /* var int that contains 20 ints--> 20*4 bytes = 80*/
    int num[N];
    int acumulado = 0;

    /*Print all values in memory position (1-20)*/
    for (int i=1; i<=N; i++) {
        num[i] = i*i;
        /*Save in 'acumulado' the sum of total values*/
        acumulado += num[i];
        printf ( "Posición %i guarda: %i\nAcumulado: %i\n", i, num[i], acumulado );
    }
    return EXIT_SUCCESS;
}

