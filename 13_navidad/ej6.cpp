#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void banner ( char titulo[] ) {
    char comando[50] = "toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( "clear" );
    system ( comando );
}

int main (int argc, char *argv[]) {

    /*Banner title*/
    char titulo[] = "Array Copia";
    banner ( titulo );
    /* Finish Banner*/

    /* Copy the value of 'origen' to 'destino'*/

    const char *origen = "Feliz Navidad";
    char destino[100];
    /*Use strcpy function to copy the string*/
    strcpy (destino, origen);

    printf ( "Array 'constante': %s\nVariable 'destino': %s\n", origen, destino );
    return EXIT_SUCCESS;
}

