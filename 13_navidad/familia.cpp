#include <stdio.h>
#include <stdlib.h>

#define PAPUP 8
#define MAMUP 4
#define HIJOP 2
#define HIJAP 1

enum TMiembro {
    papa,
    mama,
    hijo,
    hija,
    total_miembros
};

const char *miembros[] = {
    "Papá",
    "Mamá",
    "Hijo",
    "Hija"
};

int conmutar (int old_status, enum TMiembro miembro) {
    int new_status, bit;
    switch (miembro) {
        case papa:
            bit = PAPUP;
            break;
        case mama:
            bit = MAMUP;
            break;
        case hijo:
            bit = HIJOP;
            break;
        case hija:
            bit = HIJAP;
            break;
        default:
            bit = 0;

    }
    new_status = old_status ^ bit;
    return new_status;
}

/* acostar: devuelve el valor old_status habiéndo puesto a 0 el bit correspondiente
   al miembro de la familia */
int acostar (int old_status, enum TMiembro miembro) {
    int bit;
    switch (miembro) {
        case papa:
            bit = PAPUP;
            break;
        case mama:
            bit = MAMUP;
            break;
        case hija:
            bit = HIJAP;
            break;
        case hijo:
            bit = HIJOP;
            break;
        default:
            bit = 0;
    }
    return old_status & ~bit;
}

/* levantar: devuelve el valor old_status habiéndo levantado el bit correspondiente
   al miembro de la familia */
int levantar (int old_status, enum TMiembro levantado) {
    int bit;
    switch (levantado) {
        /* Rellena todos los casos */
        case papa:
            bit = PAPUP;
            break;
        case mama:
            bit = MAMUP;
            break;
        case hija:
            bit = HIJAP;
            break;
        case hijo:
            bit = HIJOP;
            break;
        default:
            bit = 0;
    }
    return old_status | bit;
}

/* Imprime con texto el estado de cada miembro de la familia*/
void print (int waked) {
   int i=0, miembro = 1;
    do {
        printf ("%s está %s\n",
                miembros[i],
                waked & miembro ? "levantado": "acostado");
        miembro = miembro << 1;
        i++;

    } while ( i<total_miembros);
}


int main () {
    int waked = 0;
    enum TMiembro despertado = papa;

    waked = levantar (waked, papa);
    print (waked);

    return EXIT_SUCCESS;
}

