#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void banner ( char titulo[] ) {
    char comando[50] = "toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( "clear" );
    system ( comando );
}

int main (int argc, char *argv[]) {

    /*Banner title*/

    char titulo[] = "Bucle While";
    banner ( titulo );

    /* Finish Banner*/

    int i = 1;
    while ( i>0 && i<11 ){
        printf ("La variable tiene valor: %i\nIntroduce nuevo valor:\n", i);
        scanf ( " %i", &i );

    }
    printf ("La variable no se encuentra en el rango 1-10, vale %i\n", i);

    return EXIT_SUCCESS;
}

