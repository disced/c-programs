#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 10
/**
 * @brief Imprime un banner como titulo del programa
 * @details Se llama a la función y se pasa como parametro un/a char/frase
 *          se une al final del array 'comando' el parametro 'titulo', y se
 *          ejecuta el comando mediante la función 'system'
 *
 * @return Void
 * @param titulo Puntero constante de tipo char
 * @var comando Array char con el comando a ejecutar
 */
void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}


int
main (int argc, char *argv[]) {
    banner ( "Escanea" );

    char input[MAX];
    int entero[MAX];
    for (int i=0; i<MAX; i++) {
        scanf ( "%[0-9]", &input[i] );
        entero[i] = atoi ( &input[i] );
    }
    for (int i=0; i<MAX; i++)
        printf( "%i\n", entero[i] );

    return EXIT_SUCCESS;
}
