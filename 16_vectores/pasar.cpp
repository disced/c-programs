#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define F 3
#define C 3
#define TOT F*C
/**
 * @brief Imprime un banner como titulo del programa
 * @details Se llama a la función y se pasa como parametro un/a char/frase
 *          se une al final del array 'comando' el parametro 'titulo', y se
 *          ejecuta el comando mediante la función 'system'
 *
 * @return Void
 * @param titulo Puntero constante de tipo char
 * @var comando Array char con el comando a ejecutar
 */
void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}

int tell_v (int v[TOT], int pos ) {
    return v[pos];
    // A la funcion le vamos a pasar un vector[9], pero que no tiene nada
    // también le pasamos la posicion que deseamos saber del vector
    //
    // Si lo haríamos fuera de la función deberiamos especificar la posicion
    // de la siguiente forma v[1][2], y en la función es v[5]
}


int
main (int argc, char *argv[]) {
    banner ( "Vector" );

    int v[F][C] = {
        {1,2,3},
        {4,5,6},
        {7,8,9}
    }, res;


    // Imprime cada posición
    for (int i=0; i<(TOT); i++){
        res = tell_v (v[0], i);
        printf ("%i\n", res);
    }

    return EXIT_SUCCESS;
}
