#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/**
 * @brief Imprime un banner como titulo del programa
 * @details Se llama a la función y se pasa como parametro un/a char/frase
 *          se une al final del array 'comando' el parametro 'titulo', y se
 *          ejecuta el comando mediante la función 'system'
 *
 * @return Void
 * @param titulo Puntero constante de tipo char
 * @var comando Array char con el comando a ejecutar
 */
void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}


int
main (int argc, char *argv[]) {
    banner ( "Lee Vector" );

    double size;
    double *ptr;
    printf ( "Tamaño Vetcor:\n" );
    scanf ( " %lf", &size );                   // Scan vector size, size=2 ---> (3,4), size=3 --->(3,4,7)...

    ptr = (double*) malloc(  size*sizeof(double) ); // Allocate memory

    for (int i=0; i<size; i++) {
        printf ( "Posicion %i del vector:\n", i );
        scanf (" %lf", &ptr[i]);
    }
    // ¿Si se hace con aritmetica de punters debo restar el tamaño de 'size' al finalizar la entrada de datos?
    //ptr -= size;

    printf ( "Vector: ( " );
    for (int i=0; i<size; i++)
        printf("%lf ", ptr[i]);
    printf ( ")\n" );

    free (ptr);
    return EXIT_SUCCESS;
}
