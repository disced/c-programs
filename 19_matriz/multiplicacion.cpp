#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common.h"       // Común
#include "calc_mul.h"     // Cálculos
#include "io.h"           // Entrada/Salida
void
banner ( const char *titulo ) {
    char comando[200] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}
int
main (int argc, char *argv[]) {
    banner ( "Matriz 3x3" );
    double first   [D][D],
           second  [D][D],
           result  [D][D];

    // Entrada
    ingresa_m ( first, 1 );
    ingresa_m ( second, 2 );

    // Calculos
    multiplica( first, second, result );

    // Imprime
    banner ( "Matriz 3x3" );

    imprime_m ( first, "Primera Matriz" );
    imprime_m ( second, "Segunda Matriz" );
    imprime_m ( result, "Resultado" );

    return EXIT_SUCCESS;
}
