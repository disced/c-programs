#include <stdlib.h>
#include "io.h"
#include "calc.h"

main (int argc, char *argv[]) {
    unsigned m, k, n;
    double *first, *second, *result;

    // Entrada
    ask_size ( &m, 'm' );
    ask_size ( &k, 'k' );
    ask_size ( &n, 'n' );

    defmatrix ( &first, m, k );
    defmatrix ( &second, k, n );
    defresult ( &result, m, n );

    // Cálculos
    calcmatrix ( first, second, result, m, k, n );

    // Printa matriz
    banner ( "Dimension Desconocida" );

    pmname( 'A' );
    showmatrix ( first, m, k );

    pmname( 'B' );
    showmatrix ( second, k, n );

    pmname( 'C' );
    showmatrix ( result, m, n );

    free ( first  );
    free ( second );
    free ( result );

    return EXIT_SUCCESS;
}
