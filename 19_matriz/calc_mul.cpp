#include "common.h"
#include "calc_mul.h"

void
multiplica ( double frist[D][D], double second[D][D], double result[D][D] ) {
    int tmp_res;
    for ( int i=0; i<D; i++ ) {
        for ( int j=0; j<D; j++ ) {
           tmp_res = 0;
           for ( int k=0; k<D; k++ )
               tmp_res += frist[i][k] * second[k][j];
           result[i][j] = tmp_res;
        }
    }
}
