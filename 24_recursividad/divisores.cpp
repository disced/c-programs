#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define BL printf ( "\n" )

void
banner ( const char *title ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

int
divisors ( int num, int ini ) {
    // if ( ini > num )           // Más lento, imprime n al final
    if ( ini > (num / 2) )        // Más rapido, no imprime n al final
        return ini;

    if ( (num % ini) == 0 )
        printf ( "%i ", ini );

    divisors ( num, ini + 1 );
}

int
main ( int argc, char *argv[] ) {
    banner ( " ' < <  Divisores de N  > > ' " );
    int num = atoi ( argv[1] ),
        ini = 1;

    divisors ( num, ini );
    BL;

    return EXIT_SUCCESS;
}
