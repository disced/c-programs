#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 135
#define F  printf ( "Fizz" )
#define B  printf ( "Buzz" )
#define FB printf ( "FizzBuzz" )

void
banner ( const char *title ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

void
fibuzz ( int n )  {
    if ( n == 0 )
        return;
    fibuzz ( n-1 );

    if ( n % 3 == 0 )               F;
    if ( n % 5 == 0 )               B;
    if ( n % 3 == 0 && n % 5 == 0 ) FB;
    if ( n % 3 != 0 && n % 5 != 0 )
        printf ( "-" );
}

int
main ( int argc, char *argv[] ) {
    banner ( " ' < < <  FizzBuzz  > > > ' " );

    fibuzz(MAX);
    printf( "\n" );

    return EXIT_SUCCESS;
}
