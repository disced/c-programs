#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void
banner ( const char *titulo ) {
    char comando[250] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}

long unsigned
suma ( long unsigned n ) {
    if ( n == 0)      // Condicional de parada
        return n;
    return n + suma ( n-1 );
}

void
asknum ( long unsigned *num ) {
    printf ( "Introduce un numero\n" );
    scanf ( "%li", num );
}

int
main (int argc, char *argv[]) {
    banner ( "Suma Recursiva" );
    // Suma todos los numeros naturales hasta N
    long unsigned numero, resultado;

    asknum ( &numero );
    resultado = suma ( numero );

    printf ( "%li = %li\n", numero, resultado );


    return EXIT_SUCCESS;
}
