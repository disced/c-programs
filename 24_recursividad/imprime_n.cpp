#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}

unsigned
imprime ( unsigned num ) {
    if ( num == 0)
        return num;
    printf ( "%i\n", num );
    imprime( num-1 );
}

int
main (int argc, char *argv[]) {
    banner ( "Imprime N" );

    // Llamamos al programa de la siguiente manera:
    // $: imprime_n 23

    unsigned num = atoi ( argv[1] );
    imprime ( num );

    return EXIT_SUCCESS;
}
