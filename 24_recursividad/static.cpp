#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 15
void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}

int
estatica () {
    // 'numero' es una variable local de la función 'estatica'
    // pero al decir que es 'static' cuando termine de ejecutarse
    // la función, la variable seguirá existiendo.
    //
    // Al ser local, desde otra función no se puede acceder a ella
    // ya que no existe, solo existe en esta función.
    static int numero = 0;
    return ++numero;
}


int
main (int argc, char *argv[]) {
    banner ( "Static" );

    for ( int i=0; i<N; i++)
        printf ( "Num %i -> %i\n", i, estatica() );
    printf ( "\n" );

    // Esto no se puede hacer:
    // printf ( "%i\n", numero );

    return EXIT_SUCCESS;
}
