#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>

#define R 4
#define C 10
#define DECI 10
#define MAX 9999

#define BL printf ("\n")
int num_falg = 0, language;

void
banner ( const char *title ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

void
ord ( int num, int row, const char *ordinal[R][C] ) {
    if ( num < 1 )
        return;

    ord ( num / DECI, row + 1, ordinal );
    printf ( "%s ", ordinal[row][num % DECI] );
}

void
p_usage ( FILE *file, char *name ) {

    fprintf ( file, "\
            Usar:       %s -[s/e][n] <numero>\n\
            %s -sn 321\n\
            %s -en 23\n\n\
            Argumentos\n\
            -s / -e    Idiomas: Spanish / English\n\
            -n         Number: 1-%i\n\
            -h         Help\n\n", name, name, name, MAX );

}

int
usage ( int argc, char **argv ) {
    int opc, num;
    char *name = argv[0];
    opterr = 0;

    while ( (opc = getopt ( argc, argv, "sehn:" )) != -1 ) {
        switch (opc) {
            case 's':
                language = 's';
                break;
            case 'e':
                language = 'e';
                break;
            case 'n':
                num = atoi ( optarg );
                num_falg = 1;
                break;
            case 'h':
                p_usage ( stdout, name );
                num_falg = 1;
                break;
            case '?':
                if ( optopt == 'n' )
                    fprintf ( stderr, "El argumento -n debe incluir un numero a continuación: -n 323\n\n");
                break;
            default:
                p_usage (stderr, name );
        }
    }
    return num;
}

int
main ( int argc, char *argv[] ) {

    const char *ord_spa[R][C] = {
        { "", "primero", "segundo", "tercero", "cuarto", "quinto", "sexto", "séptimo", "octavo", "noveno" },
        { "", "décimo", "vigésimo", "trigésimo", "cuatrigésimo", "quincuagésimo", "sexagésimo", "septuagésimo", "octogésimo", "nonagésimo" },
        { "", "centésimo", "bicentésimo", "tricentésimo", "cuadrigentésimo", "quingentésimo", "sexgentésino", "septingentésimo", "octingentésimo", "noningentésimo" },
        { "", "milesimo", "dosmilesimo", "tresmilesimo", "cuatromilesimo", "cincomilesimo", "seismilesimo", "sietemilesimo", "ochomilesimo", "nuevemilesimo" }
    };
    const char *ord_eng[R][C] = {
        { "", "first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth" },
        { "", "tenth", "twentieth", "thirtieth", "fortieth", "fiftieth", "sixtieth", "seventieth", "eightieth", "ninetieth" },
        { "", "one hundred", "two hundred", "three hundred", "four hundred", "five hundred", "six hundred", "seven hundred", "eight hundred", "nine hundred" },
        { "", "one thousand", "two thousand", "three thousand", "four thousand", "five thousand", "six thousand", "seven thousand", "eight thousand", "nine thousand" }
    };

    banner ( " ' < < <  Ordinal  > > > ' " );
    int num;
    char *name = argv[0];

    num = usage (argc, argv);

    if ( language == 's' && num_falg == 1 )
        ord ( num, 0, ord_spa );
    else if ( language == 'e' && num_falg == 1 )
        ord ( num, 0, ord_eng );
    else if ( num_falg == 0 ) {
        p_usage ( stdout, name );
    }
    BL;

    return EXIT_SUCCESS;
}
