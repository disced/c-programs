#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>

#define TITLE banner ( " ' < < <  Numero e  > > > ' " )
#define ERR .0001

void
banner ( const char *title ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

double
factorial ( int num ) {
    if ( num < 1 )       // Calcula num!, el factorial de num.
        return 1;
    return num * factorial ( num - 1 );
}

double
e ( double n ) {
    double distancia = .1 / factorial(n);

    if ( distancia < ERR )
        return distancia;
    return distancia + e( n + 1 );
}

int
main ( int argc, char *argv[] ) {
    double num, res;

    TITLE;

    // Ejecuta si hemos pasado numero como parametro.
    res = e ( 0 );

    TITLE;
    printf ( "\te(%.0lf) es:\n\n\t%.20lf\n\n", num, res );

    return EXIT_SUCCESS;
}
