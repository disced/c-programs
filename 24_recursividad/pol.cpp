#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


/*

ask_pol(n)

s(4)  ->    x⁰/0! - x¹/1! +  x²/2!   - x³/3!  +   x⁴/4!
           --------------------------------------------
pol[4]->     0        1        2         3          4
           --------------------------------------------
real  ->     1    -  x¹/1    + x²/2  -  x³/6  +   x⁴/24

*/


void
banner ( void ) {
    char command[] = "clear ; toilet -fpagga -F border:border:crop -t '< < < Polinomio > > >'";
    system ( command ) ;
}
int
factorial ( int n ) {
    if ( n == 0 )
        return 1;

    return n * factorial ( n-1 );
}

int
ask_pol ( double **pol ) {
    unsigned num;
    printf ( "Introduce n de S(n)\n");
    scanf ( "%u", &num );

    for ( int i=0; i<=num; i++ ) {
        *pol = (double*) realloc ( *pol,  (i + 1) * sizeof(double*) );
        (*(*pol + i )) = i;
    }
    return ++num;
}
void
s ( unsigned n, double *pol ) {
    double nuevo = pow ( -1, n ) / factorial ( n );

    pol[n] = nuevo;

    if ( n > 0 )
        s ( n-1, pol );
}

int
calc_pol ( int *pol, int size, int x ) {
    static double res = 0;
    if ( size < 0 )
        return res;

}

int
main ( int argc, char *argv[] ) {
    double *pol = NULL;
    int pol_size;

    pol_size = ask_pol ( &pol );
    s ( pol_size, pol);
    //ask_limits ( &li, &ls );



    for (int i=0; i<pol_size; i++)
        printf ( "%.3lf ", pol[i] );
    printf("\n");




    free ( pol );

    return EXIT_SUCCESS;
}
