#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>
/**
 * @brief Imprime un banner como titulo del programa
 * @details Se llama a la función y se pasa como parametro un/a char/frase
 *          se une al final del array 'comando' el parametro 'titulo', y se
 *          ejecuta el comando mediante la función 'system'
 *
 * @return Void
 * @param titulo Puntero constante de tipo char
 * @var comando Array char con el comando a ejecutar
 */
void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}
void show (char **lista, int s) {
    for (int i=0; i<s; i++)
        printf("%s\n", lista[i] );
    printf("\n");
}


int
main (int argc, char *argv[]) {
    banner ( "Palabras" );
    char **user_words = NULL;
    int summit = 0, total = 0;

    do {
        user_words = (char **) realloc ( user_words, (summit+1) * sizeof(char *) );
        total = scanf ( "%m[^\n]", &user_words[summit++] );
        __fpurge(stdin);
    } while ( total != 0 );

    user_words = (char **) realloc ( user_words, (--summit) * sizeof(char *) );

    show (user_words, summit);



    for (int i=0; i<summit; i++)
        free(user_words[i]);
    free(user_words);

    return EXIT_SUCCESS;
}
