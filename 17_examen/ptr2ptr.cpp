#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/**
 * @brief Imprime un banner como titulo del programa
 * @details Se llama a la función y se pasa como parametro un/a char/frase
 *          se une al final del array 'comando' el parametro 'titulo', y se
 *          ejecuta el comando mediante la función 'system'
 *
 * @return Void
 * @param titulo Puntero constante de tipo char
 * @var comando Array char con el comando a ejecutar
 */
void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}


int
main (int argc, char *argv[]) {
    banner ( "ptr2ptr" );
    char **lista = NULL;
    lista = (char **) realloc ( lista, 2 * sizeof(char *) );
    *lista = (char *) malloc (sizeof(char));
    **lista = 'h';

    printf("%c\n", *lista[0]);

    const char *nombres[] = {
        "Juan",
        "Ramon",
        "Pepe"
    };

    printf ("%s", nombres[1]);
    return EXIT_SUCCESS;
}
