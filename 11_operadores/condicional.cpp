
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef NUM //Si la constante NUM no está definida pasa a valer 3 (para poder definirla cuando ejecutamos el programa)
#define NUM 3
#endif

void banner ( char titulo[] ) {
    char comando[50] = "toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( "clear" );
    system ( comando );
}

int main (int argc, char *argv[]) {

    /*Banner title*/

    char titulo[] = "Condicional";
    banner ( titulo );

    /* Finish Banner*/


    char resultado = NUM% 2 == 0 ? 'p' : 'i';

    printf ( "%i => %c\n", NUM, resultado );


    return EXIT_SUCCESS;
}

