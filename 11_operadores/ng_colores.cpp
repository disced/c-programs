#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * Define ANSI colours
 */
#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define BLUE "\x1b[34m"

#define CYAN "\x1b[36m"
#define MAGENTA "\x1b[35m"
#define YELLOW "\x1b[33m"

#define BLACK "\x1b[30m"
#define WHITE "\x1b[37m"

void banner ( char titulo[] ) {
    char comando[50] = "toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( "clear" );
    system ( comando );
}

int main (int argc, char *argv[]) {

    /*Banner title*/
    char titulo[] = "Colores";
    banner ( titulo );
    /* Finish Banner*/

    int BinColor;
    const char *eleccion[] = {
        "ROJO",
        "VERDE",
        "AZUL",
        "CYAN",
        "AMARILLO",
        "MAGENTA",
        "BLANCO",
        "NEGRO"
    };
    /**
     * Input block
     */
    printf ( "Elección %sRed %sGreen %sBlue\n%sEjemplo: activar el rojo y azul: 101\n",
        RED,
        GREEN,
        BLUE,
        WHITE  );
    scanf ( " %i", &BinColor);


    return EXIT_SUCCESS;
}

