#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/*
 * Define Macros
 * Macro Variadica
 */
#define P(...) printf ( __VA_ARGS__ );

void banner ( char titulo[] ) {
    char comando[50] = "toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( "clear" );
    system ( comando );
}

int main (int argc, char *argv[]) {

    /*Banner title*/
    char titulo[] = "Operadores";
    banner ( titulo );
    /* Finish Banner*/

    int b = 37;
    P("Valor inicial b: %i\n", b);
    b %= 5;

    /**
     * Llamada a la macro con cualquier valor
     */
    P("Modulo de 37 entre 5 = %i\n", b);

    b <<=2;
    P("<<: %i\n", b);


    b >>=2;
    P(">>: %i\n", b);
    return EXIT_SUCCESS;
}

