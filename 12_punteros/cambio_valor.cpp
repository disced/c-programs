#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char banner ( char titulo[] ) {
    char comando[50] = "toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( "clear" );
    system ( comando );
}

int main (int argc, char *argv[]) {

    /*Banner title*/

    char titulo[] = "Aritmetica Punteros";
    banner ( titulo );

    /* Finish Banner*/

    int nota = 8;                 // Create var nota int (4bytes)
    int *PunteroNota = &nota;     //Create var pointer that save memory direction of 'nota'

    // Print 'nota', saved direction in pointer 'PunteroNota' and value of direction memory '*PunteroNota'
    printf ( "Variable nota: %i\n\
            Puntero: %p , Valor Puntero %i\n", nota, PunteroNota, *PunteroNota);

    // Add to value of *PunteroNota 1 this is eq to nota++
    (*PunteroNota)++;

    printf ( "Variable nota: %i\n\
            Puntero: %p , Valor Puntero %i\n", nota, PunteroNota, *PunteroNota);


    // If you do PunteroNota++, add to direction memory 4 bytes  
    PunteroNota++;
    printf ( "\t    Puntero++: %p\n", PunteroNota );

    printf ( "%p\n", PunteroNota );

    return EXIT_SUCCESS;
}

