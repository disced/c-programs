#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cpersona.h"

void
banner () {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t '< < < SKEL > > >'";
    system ( command );
}

int
main ( int argc, char *argv[] ) {
    banner();

    CPersona persona ( "Jhon", "Smith", 31 );
    printf ( "%s\n", persona.getNombre() );
    persona.setNombre ( "Jonathan" );
    persona.display();

    return EXIT_SUCCESS;
}
