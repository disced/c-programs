#include <string.h>
#include <stdio.h>
#include <iostream>
#include "ccaballo.h"

CCaballo::CCaballo
  (const char *color, bool manchas, unsigned extrem, TipoPelo pelo):
CMamifero (extrem, pelo),
manchas (manchas)
{
  strcpy (this->color, color);
}


CCaballo::CCaballo
  (const char *color, bool manchas, unsigned extrem, double temp,
   TipoPelo pelo):
CMamifero (extrem, temp, pelo),
manchas (manchas)
{
  strcpy (this->color, color);
}

CCaballo::CCaballo
  (const char *color, bool manchas, unsigned extrem, TipoCaballo caballo,
   TipoPelo pelo):
CMamifero (extrem, pelo), manchas (manchas),
TCaballo (caballo)
{
  strcpy (this->color, color);
}

void
CCaballo::relincha ()
{
  std::cout << "El caballo relincha." << std::endl;
}

void
CCaballo::anda ()
{
  std::cout << "Moviendo el Caballo sin posición" << std::endl;
}

void
CCaballo::anda (double x, double y)
{
  printf
    ("El Caballo se mueve a la posición:\n\tx: %5.1lf\n\ty: %5.1lf\n\n",
     x, y);
}
