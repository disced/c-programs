#include "cmamifero.h"

CMamifero::CMamifero (unsigned ext, TipoPelo pelo):
n_extremidades (ext),
tipoPelo (pelo)
{
}

CMamifero::CMamifero (unsigned ext, double temp, TipoPelo pelo):
n_extremidades (ext),
temperatura (temp),
tipoPelo (pelo)
{
}

CMamifero::~CMamifero ()
{
}

double
CMamifero::getTempFah ()
{
  return this->temperatura;
}

double
CMamifero::getTempCel ()
{
  return (this->temperatura - 32) * 5 / 9;
}

double
CMamifero::getTempKel ()
{
  return (this->temperatura + 459.67) * 5 / 9;
}
