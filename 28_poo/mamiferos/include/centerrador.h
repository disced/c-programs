#ifndef __CENTERRADOR_H__
#define __CENTERRADOR_H__

#include "ccaballo.h"
#include "cpersona.h"

class CEnterrador {

    // No se si hay que crear un constructor, y
    // no se como pasar una referencia del enterrador
    // a cada mamifer.

    public:
        void entierra (CPersona persona);
        void entierra (CCaballo caballo);
};

#endif
