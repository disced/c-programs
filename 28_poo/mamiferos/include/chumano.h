#ifndef __CHUMANO_H__
#define __CHUMANO_H__

#include "cmamifero.h"

class CHumano:public CMamifero
//               ^ Hereda todo de CMamifero.
{
  char colorOjos[3];		// RGB ex: 80A ( #8800AA )

public:
    CHumano () = delete;
    CHumano (unsigned extrem, const char *cOjos, TipoPelo tPelo =
	     TipoPelo::Escaso);
    CHumano (unsigned extrem, double temp, const char *cOjos, TipoPelo tPelo =
	     TipoPelo::Escaso);



  void anda () override;
  void anda (double x, double y) override;

};

#endif
