#ifndef __CCABALLO_H__
#define __CCABALLO_H__

#include "cmamifero.h"

enum class TipoCaballo
{ Pony, Arabe, Enano };

class CCaballo:public CMamifero
{
  char color[3];
  bool manchas;
  TipoCaballo TCaballo;

public:

  // Constructores
    CCaballo () = delete;
    CCaballo (const char *color,
	      bool manchas,
	      unsigned extremidades, TipoPelo pelo = TipoPelo::Medio);

    CCaballo (const char *color,
	      bool manchas,
	      unsigned extremidades, double temp, TipoPelo pelo = TipoPelo::Medio);

    CCaballo (const char *color,
	      bool manchas,
	      unsigned extremidades,
	      TipoCaballo TCaballo, TipoPelo pelo = TipoPelo::Medio);

  // Metodos
  void relincha ();
  void anda () override;
  void anda (double x, double y) override;

};

#endif
