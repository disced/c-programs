#ifndef __CPERSONA_H__
#define __CPERSONA_H__

#define M 0x50

// Lo mismo que struct 1 diferencia.
// Por defecto:
// class  -> Privado
// struct -> Publico
//
class CPersona {
    char nombre[M];
    char apellido[M];
    unsigned edad;

    public:

    // Es el constructor por defecto que crea C++.
    // Si no lo usamos hay que eliminarlo.
    CPersona () = delete;
    CPersona ( const char *n, const char *ap, unsigned ed);
    void display ();
    void setNombre( const char *n );
    const char* getNombre ();

};

#endif
