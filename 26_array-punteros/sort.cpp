#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void
banner ( const char *title ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

void
print_list ( const char *list[], int size, const char *title ) {
    printf ( "Lista %s\n\n\t", title );
    for (int i=0; i<size; i++ )
        printf ( "%s ", list[i] );
    printf ( "\n\n\n" );
}

void
sort_words ( const char *list[], int size ) {
    int pasadas = 0, rvcmp;
    const char *buff;
    while ( pasadas <= size ) {
        for ( int i=0; i<size-1; i++ ) {
            rvcmp = strcmp ( list[i], list[i+1] );
            buff = list[i+1];

            if ( rvcmp > 0 ) {           // l[1] > l[2]
                list[i+1] = list[i];
                list[i] = buff;
            }
        }
        pasadas++;
    }
}

int
main ( int argc, char *argv[] ) {
    banner ( " ' < < <  Sort Words  > > > ' " );
    const char *list[] = {
        "casa",
        "arbol",
        "balcon",
        "abjea",
        "abejorro",
        "aba",
        "conde",
        "aba" };

    int size = sizeof(list) / sizeof(char*);

    print_list ( list, size, "desordenada:" );
    sort_words ( list, size );
    print_list ( list, size, "ordenada:" );


    return EXIT_SUCCESS;
}
