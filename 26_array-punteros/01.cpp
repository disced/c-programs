#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void
banner ( const char *titulo ) {
    char comando[300] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}

void
print_ar ( int nums[], int size, const char *titulo ) {
    printf ( "\t%s\n\t", titulo );
    for ( int i=0; i<size; i++ )
        printf ( "%i ", nums[i] );
    printf ( "\n" );
}
void
print_ptr ( int **nums, int size, const char *titulo ) {
    printf ( "\n\t%s\n\t", titulo );
    for ( int i=0; i<size; i++ )
        printf ( "%i ", *nums[i] );
    printf ( "\n" );

}

int
main (int argc, char *argv[]) {
    banner ( "Array y Punteros" );
    int n[] = { 3, 1, 2, 5, 8, 6, 7, 4 };
    const int size = sizeof(n) / sizeof(int);
    int temp,
        *m[size];
    /*
            Dibujo Algoritmo
      +-------------------+
      | 0 | 1 | 2 | 3 | 4 | <----  Posiciones
      |-------------------|
      | 2 | 4 | 1 | 3 | 5 | <----  Valores de 'n[]'
      +-------------------+

      n₀ < n₀ = 0 ⎫
      n₀ < n₁ = 1 ⎪     ⎪
      n₀ < n₂ = 0 ⎬ = 3 ⎪ size - r - 1 = 1 | m₁ = n₀
      n₀ < n₃ = 1 ⎪   r ⎪  5     3
      n₀ < n₄ = 1 ⎭

    */

    // Algoritmo

    for ( int i=0; i<size; i++ ) {
        temp = 0;
        for ( int j=0; j<size; j++ )
            if ( n[i] < n[j] )
                temp += 1;
        m[size - temp -1] = &n[i];
    }

    print_ar  ( n, size, "Array Inicial" );
    print_ptr ( m, size, "Puntero al Array" );


    return EXIT_SUCCESS;
}
